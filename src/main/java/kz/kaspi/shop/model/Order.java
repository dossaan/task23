package kz.kaspi.shop.model;

import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;

public class Order {
    private Long id;
    private Long orderNumber;
    private String clientName;
    private Double sum;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate date;

    public Order(Long id, Long orderNumber, String clientName, Double sum, LocalDate date) {
        this.id = id;
        this.orderNumber = orderNumber;
        this.clientName = clientName;
        this.sum = sum;
        this.date = date;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(Long orderNumber) {
        this.orderNumber = orderNumber;
    }

    public String getClientName() {
        return clientName;
    }

    public void setClientName(String clientName) {
        this.clientName = clientName;
    }

    public Double getSum() {
        return sum;
    }

    public void setSum(Double sum) {
        this.sum = sum;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return "Order{" +
                "id=" + id +
                ", orderNumber=" + orderNumber +
                ", clientName='" + clientName + '\'' +
                ", sum=" + sum +
                ", date=" + date +
                '}';
    }
}
