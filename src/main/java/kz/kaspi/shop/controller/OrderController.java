package kz.kaspi.shop.controller;

import kz.kaspi.shop.model.Order;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@RequestMapping("/orders")
public interface OrderController {

    @GetMapping
    String listAll(Model model);

    @GetMapping("/{id}")
    String getOne(@PathVariable Long id, Model model);

    @GetMapping("/new")
    String showForm(Model model);

    @PostMapping
    String saveOrder(Order order, Model model);

    @GetMapping ("/delete/{id}")
    String delete(@PathVariable Long id, Model model);

    @GetMapping("/sort")
    String sort(Model model);
}
