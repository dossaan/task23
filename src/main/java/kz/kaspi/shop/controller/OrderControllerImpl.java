package kz.kaspi.shop.controller;

import kz.kaspi.shop.model.Order;
import kz.kaspi.shop.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;

import java.lang.reflect.Field;
import java.text.DecimalFormat;
import java.util.List;

@Controller
public class OrderControllerImpl implements OrderController{

    @Autowired
    private OrderService service;

    @Override
    public String listAll(Model model) {
        List<Order> orders = service.getAll();
        model.addAttribute("orders", orders);
        return "list";
    }

    @Override
    public String getOne(Long id, Model model) {
        Order order = service.getOrderById(id);
        model.addAttribute("order", order);
        model.addAttribute("mode", "Update");
        return "addEditForm";
    }

    @Override
    public String showForm(Model model) {
        model.addAttribute("mode", "Create");
        return "addEditForm";
    }

    @Override
    public String saveOrder(Order order, Model model) {
        if (isValid(order.getOrderNumber()) &&
                isValid(order.getSum()) &&
                isValid(order.getClientName()) &&
                isValid(order.getDate()) &&
                order.getOrderNumber() > 0 &&
                order.getSum() > 0
        ) {
            order.setSum(Double.parseDouble(new DecimalFormat("#.##").format(order.getSum()).replace(',', '.')));
            service.save(order);
            return "redirect:./";
        } else {
            return "error";
        }
    }

    @Override
    public String delete(Long id, Model model) {
        service.delete(id);
        return "redirect:../";
    }

    @Override
    public String sort(Model model) {
        List<Order> orders = service.getSortedList();
        model.addAttribute("orders", orders);
        return "list";
    }

    public boolean isValid(Object value){
        if (value != null){
            String str = value.toString();
            str = str.trim();
            return str.length() != 0 && !str.equals("0");
        }
        return false;
    }
}
