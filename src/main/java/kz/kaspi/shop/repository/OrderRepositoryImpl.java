package kz.kaspi.shop.repository;

import kz.kaspi.shop.model.Order;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class OrderRepositoryImpl implements OrderRepository{
    private final Map<Long, Order> orders = new HashMap<>();
    private boolean ascSorted = false;

    @Override
    public List<Order> getAll() {
        return new ArrayList<>(orders.values());
    }

    @Override
    public Order getOrderById(Long id) {
        return orders.get(id);
    }

    @Override
    public void save(Order order) {
        Long id;
        if(order.getId() == null){
            id = orders.size() + 1L;
            order.setId(id);
        } else {
            id = order.getId();
        }

        orders.put(id, order);
    }

    @Override
    public void delete(Long id) {
        orders.remove(id);
    }

    @Override
    public List<Order> getSortedList() {
        List<Order> result;
        result = getAll();
        if (ascSorted){
            result.sort((x, y) -> Double.compare(y.getSum(), x.getSum()));
        } else {
            result.sort((x, y) -> Double.compare(x.getSum(), y.getSum()));
        }
        ascSorted = !ascSorted;
        return result;
    }
}
