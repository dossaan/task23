package kz.kaspi.shop.repository;

import kz.kaspi.shop.model.Order;

import java.util.List;

public interface OrderRepository {
    List<Order> getAll();

    Order getOrderById(Long id);

    void save(Order order);

    void delete(Long id);

    List<Order> getSortedList();
}
