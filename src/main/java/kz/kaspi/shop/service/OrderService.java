package kz.kaspi.shop.service;

import kz.kaspi.shop.model.Order;
import kz.kaspi.shop.repository.OrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class OrderService {
    @Autowired
    private OrderRepository repository;

    public List<Order> getAll(){
        return repository.getAll();
    };

    public Order getOrderById(Long id){
        return repository.getOrderById(id);
    };

    public void save(Order order){
        repository.save(order);
    };

    public void delete(Long id){
        repository.delete(id);
    };

    public List<Order> getSortedList(){ return repository.getSortedList();};
}
