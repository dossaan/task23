<%--
  Created by IntelliJ IDEA.
  User: Yerniyaz
  Date: 04.05.2021
  Time: 18:54
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>${mode} заказ</title>
</head>
<h1>${mode} заказ</h1>
<body>
    <form method="post" action="./">
        <input type="hidden" name="id" value="${order.id}">
        <table>
            <tr>
                <td>Номер заказа</td>
                <td><input name="orderNumber" value="${order.orderNumber}" type="text"></td>
            </tr>
            <tr>
                <td>Заказчик</td>
                <td><input name="clientName" value="${order.clientName}" type="text"></td>
            </tr>
            <tr>
                <td>Сумма заказа</td>
                <td><input name="sum" value="${order.sum}" type="number" step="any"></td>
            </tr>
            <tr>
                <td>Дата заказа</td>
                <td><input name="date" value="${order.date}" type="date"></td>
            </tr>
        </table>
        <button type="submit">Сохранить</button>
        <a href="./">Отмена</a>
    </form>
</body>
</html>
