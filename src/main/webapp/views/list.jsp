
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Онлайн магазин</title>
</head>
<body>
  <h1>
    Обзор всех заказов
  </h1>
  <table>
    <tr>
      <th>ID</th>
      <th>Номер заказа</th>
      <th>Заказчик</th>
      <th><a href="<%=request.getContextPath()%>/orders/sort">Сумма заказа</a></th>
      <th>Дата</th>
      <th>Удалить</th>
    </tr>
    <c:forEach var="order" items="${orders}">
      <tr>
        <td><a href="<%=request.getContextPath()%>/orders/${order.id}">${order.id}</a></td>
        <td>${order.orderNumber}</td>
        <td>${order.clientName}</td>
        <td>${order.sum}</td>
        <td>${order.date}</td>
        <td style="display: flex; justify-content: center; align-items: center">
          <a href="<%=request.getContextPath()%>/orders/delete/${order.id}">&times;</a>
        </td>
      </tr>
    </c:forEach>
  </table>

  <a href="<%=request.getContextPath()%>/orders/new">Create new order</a>
</body>
</html>
